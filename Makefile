# This source code is released in public domain.

# Author Tomas Stary (2016) <tomichec@gmail.com> 
# Thanks for helpfull discussion to Thérèse Godefroy

# This is a Makefile to generate the PO files for automated
# translation. If you get through the trouble of creating PO file for
# your language, you will save a lot of hassles with editing plain html
# and repeated text, which also implies potential mistakes.

# Please read the manual on the page:
# http://dev.man-online.org/man7/po4a/

# This Makefile only contains commands from the manual tested for the
# esd translation.

# After the translation of the PO file has been finished, generate new
# files using: make -f Makefile.gen

# flags for the po4a-getextize
PO4A_FLAGS=-M utf-8 -f xhtml -o porefs=none \
           -o 'untranslated=<script>' \
           -o 'attributes=<meta>content' \
           --package-name emailselfdefense --package-version $version

# list of source files for the translation
SRC=confirmation.html\
	index.html\
	infographic.html\
	mac.html\
	next_steps.html\
	windows.html\
	workshops.html

############################################################
# initialise the po template from English version, this is to be used,
# there is no translation for now, and you are going to build it from
# scratch. Then copy the file esd-temp.pot to your language file
# esd-$(lang).po such as esd-cs.po, and edit it in your favourite PO
# editor (such as poedit, or emacs with po-mode).

# If you already have an old translation you can convert it to PO
# files as described bellow.
esd-temp.pot: $(foreach f,$(SRC),en/$f)
	po4a-gettextize $(PO4A_FLAGS) $(foreach f,$+,-m $f) -p $@

############################################################
# Convert existing translations into PO files.

# NOTE: You need to make sure that your translated document is formaly
# correct xhtml (there might be accidental mistakes).
# Validate your document to find errors (e.g. https://validator.w3.org)

# IMPORTANT: when you generate the PO files from the existing
# translation, the original files in the version *corresponding* to
# the translation have to be provided as a command line argument (with
# parameter -m).

# for that specify the directory where the original files are placed
DIR=en
# and the list of files that were translated within the corresponding
# version
FILES=$(SRC)

# The generation of the PO file is likely to go wrong, because of the
# mistakes which were introduced by the direct translation of the html
# files. In this case the po4a gives quite good error message with a
# reference in which file and line number to look at to find the
# mistake. Go to that file and try to correct the errors and then
# repeat the procedure.

# Don't give it up translating PO files is nicer.

# For more details and troubleshooting see:
# http://dev.man-online.org/man7/po4a/
# in section: HOWTO convert a pre-existing translation to po4a?
esd-%.po.extract: $(foreach d,$(DIR) %,$(foreach f,$(FILES),$d/$f))
	po4a-gettextize $(PO4A_FLAGS) $(foreach f,$(FILES),-m $(DIR)/$f) $(foreach f,$(FILES),-l $*/$f) -p $@
	cp $@ `basename -s .extract $@`

############################################################
# update the PO translation after the original has changed. This will
# make a new PO file automaticaly reusing identical translations, but
# the strings will be made fuzzy and need to verified manualy. Strings
# that changed need to be translated again.
esd-%.po.update: esd-%.po $(foreach f,$(SRC),en/$f)
	cp $< $@
	po4a-updatepo $(PO4A_FLAGS) $(foreach f,$(SRC),-m en/$f) -p $@; \
./gnun-add-fuzzy-diff $@ > $<

# TODO: The system could be further refined with 2 external scripts:

# - gnun-add-fuzzy-diff (from the GNUN package [4]). Very useful to locate
# a change within a string. It runs after po4a-updatepo.

# - some sort of reformatting script to get rid of the extra spaces, tabs,
# linefeeds, etc., to make sure that a spurious difference between
# index.html, mac.html and windows.html isn't going to add useless strings
# to an existing POT file, or make the POs fuzzy for no reason. There is
# such a script in the kitchen (reformat-html), but I am currently using a
# simpler one (reformat-original, attached).
