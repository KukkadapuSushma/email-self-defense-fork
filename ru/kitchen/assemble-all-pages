#!/bin/bash

## assemble-all-pages -- generate a set of HTML pages with variable parts
#                        for emailselfdefense.fsf.org

## Synopsis:             assemble-all-pages

## Description

# Each page is built from a template and one or several includes, as usual;
# in addition, several versions of a page can be built from a single
# template which contains all the variable parts, by deleting irrelevant
# text.

# The templates have inclusion markers (similar to SSI directives, except
# for the lack of "#") to indicate where the constant parts are to be
# inserted, and deletion markers to identify the borders of each deletion
# and indicate which page(s) the text between those borders belongs to.

# The script processes all the templates in the working directory and the
# pages are created in the parent directory.

# Ideally, any modifications should be done to the templates or includes,
# not to the final pages.

# Templates:  confirmation.t.html
#             index.t.html  (contains variable parts for mac and windows)
#             infographic.t.html
#             next_steps.t.html
#             workshops.t.html

# Includes:   footer.html
#             head.html  (contains 2 alternate sets of keywords)
#             javascript.html
#             translist.html

## Graphic-user-interface howto

# - Place the script in the same directory as the templates.
# - Display this directory in the file browser (do not just unfold the parent
#   directory) and double-click on the script.

# And if anything goes wrong, you can do a git reset, right?    ;-)

# ===========================================================================

set -e
set -o pipefail

# Create temporary files.
names=$(mktemp -t aap.XXXXXX)  || close_term 1
list=$(mktemp -t aap.XXXXXX)   || close_term 1
before=$(mktemp -t aap.XXXXXX) || close_term 1
after=$(mktemp -t aap.XXXXXX)  || close_term 1
trap 'rm -f "$names" "$list" "$before" "$after"' EXIT

# List all the templates in the working directory.
if ls *.t.html > $names  2>/dev/null; then
  sed -i 's,\.t\.html$,,' $names
else
  echo "*** There is no template in this directory." && close_term 1
fi

## Add the includes to the templates.

while read name; do
  # Make sure there is a blank line before the first include, otherwise
  # it will not be added properly.
  sed '1i\\n' $name.t.html > ../$name.html
  # List the includes.
  grep '^<!-- include virtual="' ../$name.html |
  sed 's%^.*include virtual="\([^"]\+\).*$%\1%' > $list
  # Add the includes.
  while read include; do
    sed "1,/^<!-- include virtual=\"$include\"/!d" ../$name.html > $before
    sed "1,/^<!-- include virtual=\"$include\"/d" ../$name.html > $after
    if [ -f "$include" ]; then
      cat $before $include $after > ../$name.html
    else
      echo "$include is missing." && close_term 1
    fi
    sed -i "/^<!-- include virtual=\"$include\"/d" ../$name.html
  done < $list
done < $names

## Create mac.html and windows.html from index.html.

cp ../index.html ../mac.html
cp ../index.html ../windows.html
# add them to the list of page names.
echo 'mac' >> $names
echo 'windows' >> $names

## Remove the irrelevant parts.

while read name ; do
  # Find out which deletions apply.
  grep '^<!-- START DELETION' ../$name.html |
  grep -v "$name" > $list || true
  sed -i 's%^<!-- START DELETION \([0-9][0-9]\),.*$%\1%' $list
  # Delete.
  while read deletion; do
    sed -i "/^<!-- START DELETION $deletion/, \
    /^<!-- END DELETION $deletion/d" ../$name.html
  done < $list
  # Remove the markers and any extra blank lines at the end of the page.
  sed -i '/^<!-- [A-Z]* DELETION/d' ../$name.html
  sed -i ':a /^\n*$/ {$d; N; ba}' ../$name.html
done < $names
